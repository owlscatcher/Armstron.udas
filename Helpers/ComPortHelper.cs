using ArmstrongUdas.Constants;
using ArmstrongUdas.Models;
using ArmstrongUdas.Helpers;

namespace ArmstrongUdas.Helpers
{
  public static class ComPortHelper
  {
    private static void DiscardBuffer(ComPort port)
    {
      port.DiscardInBuffer();
      port.DiscardOutBuffer();
    }
    public static void SendMessage(ComPort port, byte[] message)
    {
      if (!port.IsOpen)
      {
        port.Open();
        System.Console.WriteLine("Com-port: Port be open.");
      }

      port.Write(message, 0, 8);
    }

    public static byte[] ReadMessage(ComPort port)
    {
      var packageSize = port.BytesToRead;
      var bufferSize = 17;
      var buffer = new byte[bufferSize];
      var pingPkg = new byte[8];
      var dataPkg = new byte[9];

      if (packageSize > 0)
        for (var i = 0; i < packageSize; i++)
        {
          buffer[i] = (byte)port.ReadByte();
        }

      Console.WriteLine($"RECEIVE PKG: {BitConverter.ToString(buffer)}");

      if (packageSize != bufferSize)
      {
        System.Console.WriteLine($"SIZE ERROR: Package size = {packageSize}");
        return new byte[] { Bytes.SEZE_ERROR };
      }

      pingPkg = buffer[0..7];
      dataPkg = buffer[8..16];


      DiscardBuffer(port);
      return dataPkg;

    }
  }
}
