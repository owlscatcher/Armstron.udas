﻿using ArmstrongUdas.Models;

namespace ArmstrongUdas
{
  public static class Program
  {
    public static Channel Channel { get; set; }
    public static void Main(string[] args)
    {
      Console.WriteLine(Constants.TextConstants.Licence);
      Console.WriteLine(Constants.TextConstants.GreetingsIntro);
      Console.WriteLine(Constants.TextConstants.GreetingsBody);
      Console.WriteLine("\n\n");

      Console.Write("\nInput cnannel ID: ");
      var channelId = Convert.ToInt32(Console.ReadLine());

      Console.Write("\nCom-port name: ");
      var portName = Console.ReadLine();

      Channel = new Channel(channelId, portName);

      while (true)
      {

        Console.WriteLine("\n");

        Console.WriteLine($"\nAlpha Placed. PKG: {BitConverter.ToString(Channel.Packages.Fetch.FetchAlphaPlaced)})");
        Channel.OneshotDialog(Channel.Packages.Fetch.FetchAlphaPlaced);

        Console.WriteLine($"\nAlpha Combined. PKG: {BitConverter.ToString(Channel.Packages.Fetch.FetchAlphaCombined)})");
        Channel.OneshotDialog(Channel.Packages.Fetch.FetchAlphaCombined);

        Console.WriteLine($"\nBeta Placed. PKG: {BitConverter.ToString(Channel.Packages.Fetch.FetchBetaPlaced)})");
        Channel.OneshotDialog(Channel.Packages.Fetch.FetchBetaPlaced);

        Console.WriteLine($"\nBeta Combined. PKG: {BitConverter.ToString(Channel.Packages.Fetch.FetchBetaCombined)})");
        Channel.OneshotDialog(Channel.Packages.Fetch.FetchBetaCombined);

        Console.WriteLine("\n");

        Thread.Sleep(3000);
      }
    }
  }
}
