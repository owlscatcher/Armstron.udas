namespace ArmstrongUdas.Models
{
  public class WorkMode
  {
    public byte[]? SilenMode { get; set; }
    public byte[]? FrequencyMode { get; set; }
    public byte[]? TimingMode { get; set; }
    public byte[]? GetMode { get; set; }
  }

  public class Operation
  {
    public byte[]? OpenBlanker { get; set; }
    public byte[]? CloseBlanker { get; set; }
    public byte[]? StartRewind { get; set; }
    public byte[]? RewindAndBlanker { get; set; }
    public byte[]? GetResult { get; set; }
  }

  public class LightAlert
  {
    public byte[]? Normal { get; set; }
    public byte[]? Warning { get; set; }
    public byte[]? Danger { get; set; }
    public byte[]? SpecialSignal { get; set; }
  }

  public class Fetch
  {
    public byte[]? FetchARMS { get; set; }
    public byte[]? FetchAlphaCombined { get; set; }
    public byte[]? FetchAlphaPlaced { get; set; }
    public byte[]? FetchBetaCombined { get; set; }
    public byte[]? FetchBetaPlaced { get; set; }
  }

  public class Packages
  {
    public Fetch? Fetch { get; set; }
    public WorkMode? WorkMode { get; set; }
    public Operation? Operation { get; set; }
    public LightAlert? LightAlert { get; set; }
    public byte[]? SetAddress { get; set; }
  }
}
