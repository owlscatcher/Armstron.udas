using ArmstrongUdas.Helpers;
using ArmstrongUdas.Constants;

namespace ArmstrongUdas.Models
{
  public class Channel
  {
    public int ChannelId { get; set; }
    public string? Name { get; set; }
    public ComPort Port { get; set; }
    public byte[] ChannelBuffer { get; set; } = new byte[0];
    public Packages? Packages { get; set; }
    public DateTime EventDateTime { get; set; }
    public double EventImpulseValue { get; set; }
    public int EventCount { get; set; }
    public int EventErrorCount { get; set; }

    public Channel(int channelId, string comPortName)
    {
      this.ChannelId = channelId;
      this.Name = "Channel_" + channelId;
      this.Port = new ComPort(comPortName);
      this.EventCount = 0;
      this.EventErrorCount = 0;
      this.Packages = PackageHelper.GetPackages((byte)this.ChannelId);
    }

    public void SendMessage(byte[] message)
    {
      ComPortHelper.SendMessage(this.Port, message);
    }

    public void ReceiveMessage()
    {
      ChannelBuffer = ComPortHelper.ReadMessage(this.Port);
    }

    public bool SaveEventValue()
    {
      if (this.ChannelBuffer.SequenceEqual(new byte[] { Bytes.CRC_ERROR })
          || this.ChannelBuffer.SequenceEqual(new byte[] { Bytes.SEZE_ERROR }))
        return false;

      var impulses = UnitConverterHelper.ToImpulse(this.ChannelBuffer);

      if (this.EventImpulseValue == impulses)
        return false;
      else
      {
        this.EventImpulseValue = impulses;
        this.EventDateTime = DateTime.Now;

        return true;
      }
    }

    public void SetEventCount(bool isSaved)
    {
      if (isSaved)
      {
        this.EventCount++;
        this.EventErrorCount = 0;
      }
      else
        this.EventErrorCount++;
    }

    public void OneshotDialog(byte[] message)
    {
      this.Port.Open();
      this.SendMessage(message);
      Thread.Sleep(100);
      this.ReceiveMessage();
      var isSaved = this.SaveEventValue();
      SetEventCount(isSaved);

      if (isSaved)
      {
        Thread.Sleep(1000);
      }

      this.PrintChannelInfo();
      this.Port.Close();

      EventImpulseValue = 0;
    }



    public void PrintChannelInfo()
    {
      System.Console.WriteLine($"Id: {this.ChannelId}\t" +
                              $"Name: {this.Name}\t" +
                              $"Impulses: {this.EventImpulseValue:E3}\t" +
                              $"Count: {this.EventCount}\t" +
                              $"Error: {this.EventErrorCount}\t");
    }
  }
}
