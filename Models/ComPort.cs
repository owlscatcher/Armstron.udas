using System.IO.Ports;

namespace ArmstrongUdas.Models
{
  public class ComPort : SerialPort
  {
    public ComPort(string name)
    {
      PortName = String.IsNullOrWhiteSpace(name) ? throw new ArgumentNullException() : name;
      BaudRate = 9600;
    }
  }
}
